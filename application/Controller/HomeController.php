<?php


namespace Mini\Controller;

use Mini\Model\Modelo;

class HomeController
{
    

    public function index()
    {
        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/home/index.php';
        require APP . 'view/_templates/footer.php';
    }

    public function listarDepartamento(){

        $modelo = new Modelo();
        $depart = $modelo->listarDepartamento();
        echo $depart;
    }

    public function guardarData(){

        $usuario = new Modelo();
        $response = array();
        
        try{
            $departamento = $_POST["departamento"];
            $ciudad = $_POST["ciudad"];
            $nombre = $_POST["nombre"];
            $correo = $_POST["correo"];
        
            $usuario->__SET("departamento",$departamento);
            $usuario->__SET("ciudad",$ciudad);
            $usuario->__SET("nombre",$nombre);
            $usuario->__SET("correo",$correo);
    
            if($usuario->guardarData()){
                $validacion = array("estado" => 1, "msg" => 'Tu información ha sido recibida satisfactoriamente');
            }else{
                $validacion = array("estado" => 0, "msg" => 'Ocurrió un error al tratar de guardar los datos');
            }
            
            echo json_encode($validacion);
        }catch(Exception $e){

            echo "Error al guardar los datos" .  $e->getMessage();
        }
        

    }
}
