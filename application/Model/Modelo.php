<?php

namespace Mini\Model;

use Mini\Core\Model;

class Modelo extends Model {

    public function __SET($a,$v){
        $this->$a = $v;
    }

    public function __GET($a){
        return $this->$a;
    }

    private $nombre;
    private $departamento;
    private $ciudad;
    private $correo;

    public function listarDepartamento(){
        
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "http://sigma-studios.s3-us-west-2.amazonaws.com/test/colombia.json");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($ch);

        
        return json_encode($res);
        curl_close($ch);

        
    }

    public function guardarData(){

        $consulta = "INSERT INTO contacts (name,email,state,city) VALUES (?,?,?,?)";
        $stm = $this->db->prepare($consulta);
        $stm->bindParam(1,$this->nombre);
        $stm->bindParam(2,$this->correo);
        $stm->bindParam(3,$this->departamento);
        $stm->bindParam(4,$this->ciudad);
        $stm->execute();
        return $stm;

    }
}



?>