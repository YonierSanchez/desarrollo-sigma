
    <div class="container">
        <div class="row">
            <div class="panel">
                <div class="panel-body">
                    <form method="post" id="frmEnviarData">
                        <div class="col-md-12">
                            <div class="logo form-group col-md-12">
                                <img src="https://sigma-studios.s3-us-west-2.amazonaws.com/test/sigma-logo.png" alt="Pieza-Sigma"> 
                            </div>
                            <div class="texto form-group col-md-12">
                                <h3 ><strong>Prueba de desarrollo Sigma</strong></h3>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </div>
                            <div class="formulario col-md-12">
                                <div class="logo2 form-group col-md-7">
                                    <img src="https://sigma-studios.s3-us-west-2.amazonaws.com/test/sigma-image.png" alt="Pieza-Sigma"> 
                                </div>
                                <div class="formu form-group col-md-5">
                                    <div class="form-group col-md-11" id="Div1">
                                        <label class="texto">Departamento*</label>
                                        <select id="idDepartamento" class="form-control" required>
                                            <option value="">Departamento</option>
                                        </select>
                                    </div>
                                    <div class="formul form-group col-md-11">
                                        <label class="texto">Ciudad*</label>
                                        <select id="idCiudad" class="form-control"  required>
                                            <option value="">Ciudad</option>
                                        </select>
                                    </div>
                                    <div class="formul form-group col-md-11">
                                        <label class="texto">Nombre*</label>
                                        <input type="text" id="Nombre" maxlength="50" class="form-control" required placeholder="Pepito de Jesús">
                                    </div>
                                    <div class="formul form-group col-md-11">
                                        <label class="texto">Correo*</label>
                                        <input type="email" id="Correo" maxlength="30" class="form-control" required placeholder="Pepitodejesus@gmail.com">
                                    </div>
                                    <div class="formul form-group col-md-3">
                                       <button type="submit" id="Enviar" class="btn btn-enviar">ENVIAR</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>


<div class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Mensaje del Sistema</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <strong><p id="mensaje"></p></strong>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Aceptar</button>
      </div>
    </div>
  </div>
</div>
    