
let DepartamentosCiudades = [];
let CiudadesSelct = [];

$(document).ready(function(){
    Departamentos();
    
});

function Departamentos(){

    $.ajax({
        dataType:'json',
        url: uri+'Home/listarDepartamento'
    }).done((response)=>{
        DepartamentosCiudades = JSON.parse(response);
       $("#idDepartamento").html(`<option value="">Departamento</option>`);
       for (const key in DepartamentosCiudades) {
        const element = key;
        $("#idDepartamento").append(`<option value="${element}">${element}</option>`);
       }
    })
};


$("#idDepartamento").change(()=>{

    let Departamento = $("#idDepartamento").val();
    if(Departamento != ""){
        Ciudades(Departamento);
    }else{
        $("#idCiudad").html(`<option value="">Ciudad</option>`);
    }

    
})

function Ciudades(Departamento){

    CiudadesSelct = DepartamentosCiudades[`${Departamento}`];

    $("#idCiudad").html(`<option value="">Ciudad</option>`);
    for (const key in CiudadesSelct) {
        if (CiudadesSelct.hasOwnProperty(key)) {
            const element = CiudadesSelct[key];

            $("#idCiudad").append(`<option value="${element}">${element}</option>`);
            
        }
    }

}

$("#frmEnviarData").submit((event) => {

    event.preventDefault();

    let Departamento = $("#idDepartamento").val();
    let Ciudad = $("#idCiudad").val();
    let Nombre = $("#Nombre").val();
    let Correo = $("#Correo").val();

    $.ajax({
        type:'post',
        dataType:'json',
        url: uri+'Home/guardarData',
        data:{
            departamento: Departamento,
            ciudad: Ciudad,
            nombre: Nombre,
            correo: Correo
        }
    }).done((response)=>{

        resultado = response["estado"];
        mensaje = response["msg"];
        if(resultado == 1){
            $("#idDepartamento").html(`<option value="">Departamento</option>`);
            $("#idCiudad").html(`<option value="">Ciudad</option>`);
            $("#Nombre").val("");
            $("#Correo").val("");
            $('.modal').modal("show")
            $("#mensaje").html(`${mensaje}`)
        }else{
            $("#mensaje").html(`${mensaje}`)
        }
        
    });  

});
